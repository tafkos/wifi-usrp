
# Different ways to setup RFNoC development environment

There are a plenty of guides describing how to deploy RFNoC environmnet, differ by UHD release, 
sources and different package managers. So any USRP developper mostlikely faces a problem which 
way is easiest and less buggy... So do we. 

Some considerations:
* Firstly, we focus on UHD4 (as of the day it is v4.0.1.5).  
* Secondly, our target is n310 USRP (x3xx, n3xx should be similar or even no difference).  
* Thirdly, OS is Linux Ubuntu LTS 20.04 (18.04 is similar).  

RFNoC development environment includes:
* UHD driver
* GNU Radio
* gr-ettus OOT 
* Xilinx Vivado 2019.1 (this has no issues and installed as usual, like described [here](https://digilent.com/reference/programmable-logic/guides/installing-vivado-and-sdk) )

## Install prequisites
```shell
sudo apt-get -y install autoconf automake build-essential ccache cmake cpufrequtils doxygen ethtool fort77 g++ gir1.2-gtk-3.0 git gobject-introspection gpsd gpsd-clients inetutils-tools libasound2-dev libboost-all-dev libcomedi-dev libcppunit-dev libfftw3-bin libfftw3-dev libfftw3-doc libfontconfig1-dev libgmp-dev libgps-dev libgsl-dev liblog4cpp5-dev libncurses5 libncurses5-dev libpulse-dev libqt5opengl5-dev libqwt-qt5-dev libsdl1.2-dev libtool libudev-dev libusb-1.0-0 libusb-1.0-0-dev libusb-dev libxi-dev libxrender-dev libzmq3-dev libzmq5 ncurses-bin python3-cheetah python3-click python3-click-plugins python3-click-threading python3-dev python3-docutils python3-gi python3-gi-cairo python3-gps python3-lxml python3-mako  python3-numpy python3-numpy-dbg python3-opengl python3-pyqt5 python3-requests python3-scipy python3-setuptools python3-six python3-sphinx python3-yaml python3-zmq python3-ruamel.yaml swig wget
```

__TL'DR:__ Install by recommended way: [using-updated-pybombs-recommended](#using-updated-pybombs-recommended)

## apt-get install

This way is as simple as

```shell
sudo apt install uhd-host gnuradio
```

good for first and quick start, however has many limitaions:
- need to wait maintainers updates
- no source available (need to install deb-src)
- system wide install
- root access is required for any modifications 
- potential conflicts if other parts, verisons are required to be installed 
- no RFNoC OOT: gr-ettus

## Using conda
Similar to [apt-get install](#apt-get-install) but has advantages:
- can be installed w/o root if dependenies are 
- flexibly choose package revisions 
- different environments

1. Install conda by [official guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html), then
2. Create environment and install uhd v4.1.0.5 and gnuradio packages with dependenices
```shell
conda create -p /home/user/conda-gnuradio-uhd4 -c conda-forge uhd=4.1.0.5 gnuradio
```
3. activate environment (activate every new running terminal to work with UHD or GNU Radio) and run GNU Radio
```shell
conda activate /home/user/conda-gnuradio-uhd4
gnuradio-companion
```
4. Clone UHD sources to build from bitstreams and OOT modules
```shell
cd /home/user/conda-gnuradio-uhd4
clone https://github.com/EttusResearch/uhd.git --branch=v4.1.0.5 --recursive uhd4.1.0.5
```


## Build from source 

This way gives to user maximum control over all options, revisions, configurations.

Good step-by-step guide: 

https://www.linkedin.com/pulse/installation-uhd-40-gnu-radio-38-rfnoc-ubuntu-2004-murthy-s?trk=pulse-article_more-articles_related-content-card

However, has a lot of manually work. It's better to collect all steps into some single script... Next way:

## Using updated [PyBOMBS](https://github.com/gnuradio/pybombs#pybombs) (RECOMMENDED)

This way is the best (IMHO) combination of above methods. As keeps advantage of [Build from source](#build-from-source) as similar to automated package install like [Using conda](#using-conda). However, also has a disadvantage, which can be relatively easy overcome by this guide:

1) Install PyBOMBs:   
```shell
sudo apt-get install python3-pip
sudo pip3 install --upgrade git+https://github.com/gnuradio/pybombs.git
```

2) Prefix initialization and changing git revision for UHD package
```shell
pybombs -y prefix init ~/prefix-uhd4 -a uhd4
pybombs -y --prefix uhd4 auto-config
pybombs -y --prefix uhd4 recipes add-defaults
pybombs -y --prefix uhd4 config --package uhd gitrev v4.1.0.5
```
__NOTE__: that default PyBOMBS recipes are placed at `~/.pybombs/recipes/gr-recipes/`  

3) Install gr-ettus and its dependencies uhd, gnuradio using updated PyBOMBs recipe:  
```shell
pybombs -y --prefix uhd4 install gr-ettus
```
4) Download default prebuild images and check connection to USRP:
```shell
source ~/prefix-uhd4/setup_env.sh
uhd_images_downloader -t n3xx --yes
uhd_find_devices
```
__NOTE__: Every time initialize prefix before start working with UHD by command:
```shell
source ~/prefix-uhd4/setup_env.sh
```

## Using updated [PyBOMBS](https://github.com/gnuradio/pybombs#pybombs) (RECOMMENDED) for UHD-3.15.LTS

```shell
pybombs -y prefix init ~/prefix-uhd3.15 -a uhd3.15
pybombs -y --prefix uhd3.15 auto-config
pybombs -y --prefix uhd3.15 recipes add-defaults
pybombs -y --prefix uhd3.15 config --package uhd gitbranch UHD-3.15.LTS
pybombs -y --prefix uhd3.15 config --package uhd gitopt recursive
pybombs -y --prefix uhd3.15 install gr-ettus
source ~/prefix-uhd3.15/setup_env.sh
uhd_images_downloader -t n3xx --yes
uhd_find_devices
```
# Upgrade Firmware Procedure
## Update FPGA bitstream and rootfs of embbeded system

Check details at https://kb.ettus.com/USRP_N300/N310/N320/N321_Getting_Started_Guide

1. Load default FPGA image:
```shell
source ~/prefix-uhd4/setup_env.sh
uhd_image_loader --args "type=n3xx,addr=192.168.50.1,fpga=HG"
```

2. Connect from host to embedded system CLI through USB-UART cable:
```shell
sudo screen /dev/serial/by-id/usb-Silicon_Labs_CP2105_Dual_USB_to_UART_Bridge_Controller_007F6CB5-if01-port0 115200
```
3. Run following commands from embedded system CLI:
```shell
scp user@192.168.50.1:/home/user/prefix-uhd4/share/uhd/images/usrp_n3xx_fs.mender /home/root
mender install /home/root/usrp_n3xx_fs.mender
reboot
```
4. If system booted OK then commit update (running following command), otherwise after next reboot previous version will be booted:
```shell
mender commit
```


__NOTE__: For UHD 3.xx commands are slightly differs: 
| UHD4 | UHD3 |
|------|------|
|   `mender install <_fs.mender>`   |  `mender -rootfs <_fs.mender>` |
|   `mender commit`   |  `mender -commit` |


## Add existing and custom RFNOC modules and rebuild FPGA bitstream  

https://kb.ettus.com/Getting_Started_with_RFNoC_in_UHD_4.0#Example:_Adding_an_FFT_Block
